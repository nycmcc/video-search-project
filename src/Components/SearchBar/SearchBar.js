import React from 'react';

class SearchBar extends React.Component {
  state = {term: ''};

  onInputChange = (e) => {
    this.setState({term: e.target.value})
  };

  // I'm assigning this as an arrow function because this is a callback that I get a pass
  // to some child element 
  onFormSubmit = (e) => {
    e.preventDefault();

    // Todo: make sure we call callback from parent component
    this.props.onFormSubmit(this.state.term);
  }

  render() {
    return (
      <div className="search-bar ui segment">
        <form onSubmit={this.onFormSubmit} className="ui form">
          <div className="field">
            <label>Video Search</label>
            <input 
              type="text" 
              value={this.state.term}
              onChange={this.onInputChange}
              // onChange={e => this.setState({ term: e.target.value})}
            />
          </div>
        </form>
      </div>
    )
  }
}

export default SearchBar;