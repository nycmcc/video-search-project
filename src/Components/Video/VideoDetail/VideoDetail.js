import React from 'react';

// orignal format
// const VideoDetail = (props) => {
//   return <div></div>
// }

// destructing format
const VideoDetail = ({video}) => {
  // To prevent this "Uncaught TypeError: Cannot read property 'snippet' of null" 
  // (see below)
  if (!video) {
    return <div>Loading...</div>;
  }

  const videoSrc = `https://www.youtube.com/embed/${video.id.videoId}`
  
  return (
    <div>
      <div className="ui embed">
        <iframe title="videoIframe" src={videoSrc} />
      </div>
      <div className="ui segment">
        <h4 className="ui header">{video.snippet.title}</h4>
        <p>{video.snippet.description}</p>
      </div>
    </div>);
}

export default VideoDetail;