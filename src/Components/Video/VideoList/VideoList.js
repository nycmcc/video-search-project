import React from 'react';
import VideoItem from './VideoItem/VideoItem';

// Orignal Approach
// const VideoList = (props) => {
//   // props.videos => is going to be the array of different records that we want to render 
//   // out as HTML o the screen.
//   return<div>{props.videos.length}</div>
// }

// Clean Approach
// const VideoList = ({videos}) => {
//   return<div>{videos.length}</div>
// }

// Will map over this video's array. And for every video inside there we will render one 
// single video item component.
const VideoList = ({videos, onVideoSelect}) => {
  const renderedList = videos.map((video, index) => {
    return <VideoItem key={index} onVideoSelect={onVideoSelect} video={video} />;
  });
  return<div className="ui relaxed divided list">{renderedList}</div>
}

export default VideoList;