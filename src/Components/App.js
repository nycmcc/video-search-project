import React from 'react';
import youtube from '../apis/YoutubeAPI';

// components
import SearchBar from './SearchBar/SearchBar';
import VideoList from './Video/VideoList/VideoList';
import VideoDetail from './Video/VideoDetail/VideoDetail';

class App extends React.Component {
  state = {videos: [], selectedVideo: null};

  // need to do is make sure that we have some default search for this thing as opposed
  // to showing the loading text at the top.
  componentDidMount() {
    this.onTermSubmit('air');
  }

  onTermSubmit = async term => {
    // console.log(term);
    const response = await youtube.get('/search',
    {
      params: {
        q: term
      }
    });

    // console.log(response);
    // this.setState({videos: response.data.items });

    //  when a user searches for a term we'll not only update our list of videos but 
    //  we will also pick one of the videos out of that list and set it as our selected 
    //  video.

    this.setState({
      videos: response.data.items,
      selectedVideo: response.data.item
    });

  };

  onVideoSelect = (video) => {
    // Allows to select and click a video item which is nested in VideoList which is 
    //  nested in VideoItem then rendered the app to show the selected video item.
    // console.log('From the App!', video);
    this.setState({selectedVideo: video});
  }

  // in order to setup Youtube API from parent to child
  // youtube.get('/search',
  // {
  //   params: {
  //     q: term
  //   }
  // });

  render() {
    return (
      <div className="ui container">
        <SearchBar onFormSubmit={this.onTermSubmit} /> 
        {/* 
          this is a prop (callback name): onFormSubmit={this.onTermSubmit}
          also "prop" name and callback name aren't identical. can change
          "onFormSubmit" to "onTermSumbit"

          shows how many videos: I have {this.state.videos.length}
        */}
        <VideoDetail video={this.state.selectedVideo} />
        <VideoList 
          onVideoSelect={this.onVideoSelect} 
          videos={this.state.videos} 
        />
      </div>
    );
  }
}

export default App;