import axios from 'axios';

const KEY = 'AIzaSyCmctIC09YOVEkWziKm8pG098ZvYX-VCts';

// https://developers.google.com/youtube/v3/docs/search/list

// The idea here is that we are going to make a pre-configured instance of Axios that 
// already has a base URL and some default parameters loaded into it.
export default axios.create({
  baseURL: 'https://www.googleapis.com/youtube/v3',
  params: {
    part: 'snippet',
    maxResults: 5,
    key: KEY
  }
});